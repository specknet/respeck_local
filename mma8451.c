#include "mma8451.h"
#include "nrf_drv_twi.h"
#include "nrf_drv_gpiote.h"
#include "app_error.h"
#include "nrf_delay.h"


static const nrf_drv_twi_t m_twi_mma8451 = NRF_DRV_TWI_INSTANCE(0);

static volatile bool read_requested = false;
static volatile bool read_request_in_progress = false;
static volatile bool write_request_in_progress = false;
static volatile bool transient_read_requested = false;

uint8_t mma_cmd_init_standby[2] = { 0x2a, 0x2c};
uint8_t mma_cmd_control2[2] = { 0x2b, 0x01};
uint8_t mma_cmd_interrupt_enable[2] = { 0x2d, 0x40};
uint8_t mma_cmd_interrupt_config[2] = { 0x2e, 0x40};
uint8_t mma_cmd_fifo[2] = { 0x09, 0xA0 };
uint8_t mma_cmd_init_active[2] = { 0x2a, 0x2d };
uint8_t mma_read[] = {0x00};
uint8_t mma_read_fifo[] = {0x01};

uint8_t mma_cmd_transient_enable[2] = {0x1D, 0x06};
uint8_t mma_cmd_transient_disable[2] = {0x1D, 0x00};
uint8_t mma_cmd_transient_config_ths[2] = {0x1F, 0x03};
uint8_t mma_cmd_transient_config_count[2] = {0x20, 0x00};
uint8_t mma_cmd_transient_control2[2] = { 0x2b, 0x01};
uint8_t mma_cmd_transient_interrupt_enable[2] = {0x2D, 0x20};
uint8_t mma_cmd_transient_interrupt_config[2] = {0x2e, 0x20};
uint8_t mma_read_transient_src[1] = {0x1E};

mma8451_callback_t callback = NULL;

uint8_t samples[192];

bool sleep_mode = false;

/**
 * @brief TWI events handler.
 */
void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
    ret_code_t err_code;

    switch(p_event->type)
    {
        case NRF_DRV_TWI_RX_DONE:
            if (!transient_read_requested) {
    			if(callback) {
                	callback(samples, p_event->length);
    			}
            }
            transient_read_requested = false;
            read_request_in_progress = false;
            read_requested = false;
            break;
        case NRF_DRV_TWI_TX_DONE:
            if(read_requested != true)
            {
				write_request_in_progress = false;
                return;
            } else {
				write_request_in_progress = false;
	            read_request_in_progress = true;
                if (transient_read_requested) {
                    err_code = nrf_drv_twi_rx(&m_twi_mma8451, MMA8451_address, samples, 1, false);
    	            APP_ERROR_CHECK(err_code);
                } else {
    	            err_code = nrf_drv_twi_rx(&m_twi_mma8451, MMA8451_address, samples, sizeof(samples), false);
    	            APP_ERROR_CHECK(err_code);
                }
			}
            break;
        default:
            break;
    }
}

/**
 * @brief UART initialization.
 */
static void twi_init (void)
{
    ret_code_t err_code;

    err_code = nrf_drv_twi_init(&m_twi_mma8451, NULL, twi_handler, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi_mma8451);
}

int timeout = 0;

static void mma_req_read(void) {
	ret_code_t err_code;
	read_requested = true;
    timeout = 0;
    do {
        err_code = nrf_drv_twi_tx(&m_twi_mma8451, MMA8451_address, mma_read_fifo, 1, true);
        if (err_code == NRF_ERROR_BUSY) {
            nrf_delay_ms(100);
        }
        timeout++;
    } while((err_code == NRF_ERROR_BUSY) && (timeout < 10));
	APP_ERROR_CHECK(err_code);
}

static void mma_req_read_transient(void) {
    ret_code_t err_code;
	transient_read_requested = true;
    read_requested = true;
    timeout = 0;
	do {
        err_code = nrf_drv_twi_tx(&m_twi_mma8451, MMA8451_address, mma_read_transient_src, 1, true);
        if (err_code == NRF_ERROR_BUSY) {
            nrf_delay_ms(100);
        }
        timeout++;
    } while((err_code == NRF_ERROR_BUSY) && (timeout < 10));
	APP_ERROR_CHECK(err_code);
    while(transient_read_requested) {

    }
}

static void mma_command(uint8_t *command, int length)
{
	ret_code_t err_code;
    int timeout_count = 0;
	read_requested = false;
	write_request_in_progress = true;
    timeout = 0;
    do {
        err_code = nrf_drv_twi_tx(&m_twi_mma8451, MMA8451_address, command, length, false);
        if (err_code == NRF_ERROR_BUSY) {
            nrf_delay_ms(100);
        }
        timeout++;
    } while((err_code == NRF_ERROR_BUSY) && (timeout < 10));
	APP_ERROR_CHECK(err_code);
	while((write_request_in_progress == true) && (timeout_count < 1000000)) {
        timeout_count++;
		//__WFE;
	}
    write_request_in_progress = false;
}

void mma8451_start(void)
{
    nrf_drv_gpiote_in_event_disable(MMA8451_INT_PIN);
    mma_command(mma_cmd_init_standby, 2);
    sleep_mode = false;
    mma_command(mma_cmd_transient_disable, 2);
    mma_command(mma_cmd_interrupt_enable, 2);
	mma_command(mma_cmd_interrupt_config, 2);
    mma_command(mma_cmd_control2, 2);
	mma_command(mma_cmd_init_active, 2);
    nrf_drv_gpiote_in_event_enable(MMA8451_INT_PIN, true);
	//mma_req_read();
}

void mmma8451_fifo_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    if (sleep_mode) {
        callback(NULL, 0); // NULL data callback to indicate motion detected
    } else {
        mma_req_read();
    }
}

void mma8451_init(mma8451_callback_t data_callback)
{
	ret_code_t err_code;
	callback = data_callback;

    twi_init();
    nrf_delay_ms(200);

	mma_command(mma_cmd_init_active, 2);
	mma_command(mma_cmd_init_standby, 2);
	mma_command(mma_cmd_control2, 2);
	mma_command(mma_cmd_interrupt_enable, 2);
	mma_command(mma_cmd_interrupt_config, 2);
	mma_command(mma_cmd_fifo, 2);
	//mma8451_start();
	if (!nrf_drv_gpiote_is_init())
    {
        err_code = nrf_drv_gpiote_init();
        if (err_code != NRF_SUCCESS)
        {
            return;
        }
    }
	nrf_drv_gpiote_in_config_t config = GPIOTE_CONFIG_IN_SENSE_HITOLO(false);
	nrf_drv_gpiote_in_init(MMA8451_INT_PIN, &config, mmma8451_fifo_handler);
	nrf_drv_gpiote_in_event_enable(MMA8451_INT_PIN, true);
    sleep_mode = false;

}

void mma8451_init_sleep(void)
{
    sleep_mode = true;
    nrf_drv_gpiote_in_event_disable(MMA8451_INT_PIN);
    mma_command(mma_cmd_init_standby, 2);
    mma_command(mma_cmd_transient_interrupt_enable, 2);
    mma_command(mma_cmd_transient_interrupt_config, 2);
    mma_command(mma_cmd_transient_enable, 2);
    mma_command(mma_cmd_transient_config_ths, 2);
    mma_command(mma_cmd_transient_config_count, 2);
    mma_command(mma_cmd_control2, 2);
    mma_command(mma_cmd_transient_control2, 2);
    mma_command(mma_cmd_init_active, 2);
    //nrf_delay_ms(100);
    mma_req_read_transient();
    //nrf_delay_ms(1000);
    //mma_req_read_transient();
    nrf_drv_gpiote_in_event_enable(MMA8451_INT_PIN, true);
}

void mma8451_stop(void)
{
	mma_command(mma_cmd_init_standby, 2);
}
