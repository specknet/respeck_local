#include "clock.h"

#include "app_timer.h"
#include "nrf.h"
#include "nrf_soc.h"

#define MAX_RTC_COUNTER_VAL     0x00FFFFFF

static void timestamp_handler(void *p_context);

APP_TIMER_DEF(m_timestamp_id);
//static app_timer_id_t m_timestamp_id;
static volatile uint32_t timestamp = 0;
static volatile uint32_t prev_ticks;
static uint32_t ticks_setting;
uint32_t timestamp_offset = 0;

static __INLINE uint32_t rtc1_counter_get(void)
{
    return NRF_RTC1->COUNTER;
}


/**@brief Function for computing the difference between two RTC1 counter values.
 *
 * @return     Number of ticks elapsed from ticks_old to ticks_now.
 */
static __INLINE uint32_t ticks_diff_get(uint32_t ticks_now, uint32_t ticks_old)
{
    return ((ticks_now - ticks_old) & MAX_RTC_COUNTER_VAL);
}


void timestamp_init(void)
{
    uint32_t err_code;

    err_code = app_timer_create(&m_timestamp_id, APP_TIMER_MODE_REPEATED, timestamp_handler);
    timestamp_offset = 0;
    timestamp = 0;
    APP_ERROR_CHECK(err_code);
}

void timestamp_config(uint32_t ticks)
{
    uint32_t err_code;

    app_timer_stop(m_timestamp_id);

    err_code = app_timer_start(m_timestamp_id, ticks * 10000, NULL);
    ticks_setting = ticks;
    prev_ticks = rtc1_counter_get();
    //APP_ERROR_CHECK(err_code);
}

void timestamp_reset(void)
{
	timestamp = 0;
}

uint32_t timestamp_get(void)
{
    timestamp_handler(NULL);
    return timestamp + timestamp_offset;
}

static void timestamp_handler(void *p_context)
{

    timestamp += ticks_diff_get(rtc1_counter_get(), prev_ticks) / ticks_setting;
    prev_ticks = rtc1_counter_get();
}

void timestamp_offset_set(uint32_t ticks)
{
    timestamp_offset = ticks;
}
uint32_t timestamp_offset_get(void)
{
    return timestamp_offset;
}
