#ifndef _UUID_H
#define _UUID_H

#include "ble.h"

/**@brief 128-bit UUID base List. */
// e117-4bff-b00d-b20878bc3f44
#define RESPECK_BASE_UUID {{ 0x44, 0x3F, 0xBC, 0x78, 0x08, 0xB2, 0x0D, 0xB0, 0xFF, 0x4B, 0x17, 0xE1, 0x00, 0x00, 0x00, 0x00}};

#define 		RESPECK_SERVICE_UUID					0x1010

#define 		LIVE_ACCEL_DATA_UUID					0x2010
static char* 	LIVE_ACCEL_DATA_USER_DESC = 			"Live data";
#define 		TIMESTAMP_OFFSET_DATA_UUID		0x2012
static char* 	TIMESTAMP_OFFSET_DATA_USER_DESC = "Timestamp Offset";
#define     DEVICE_ID_UUID								0x2014
static char* 	DEVICE_ID_USER_DESC =             "Device ID";
#define     RESPIRATORY_INTERVALS_UUID    0x2015
static char* RESPIRATORY_INTERVALS_USER_DESC =  "Respiratory Intervals";
#define     AVERAGE_BREATHS_UUID    0x2016
static char* AVERAGE_BREATHS_USER_DESC =  "Average Breaths";

#endif
