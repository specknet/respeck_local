/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/** @file
 *
 * @defgroup ble_sdk_app_template_main main.c
 * @{
 * @ingroup ble_sdk_app_template
 * @brief Template project main file.
 *
 * This file contains a template for creating a new application. It has the code necessary to wakeup
 * from button, advertise, get a connection restart advertising on disconnect and if no new
 * connection created go back to system-off mode.
 * It can easily be used as a starting point for creating a new application, the comments identified
 * with 'YOUR_JOB' indicates where and how you can customize.
 */
#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_drv_twi.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "ble_dis.h"
#ifdef BLE_DFU_APP_SUPPORT
#include "ble_dfu.h"
#include "dfu_app_handler.h"
#endif // BLE_DFU_APP_SUPPORT
#include "ble_res.h"
#include "boards.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "device_manager.h"
#include "pstorage.h"
#include "app_trace.h"
#include "nrf_delay.h"
#include "mma8451.h"
#include "clock.h"
#include "app_fifo.h"

#include "uuid.h"

#include "breathing_rate.h"
#include "breathing.h"
#include "median_average.h"
#include "ma_stats.h"
#include "online_activity.h"


#define IS_SRVC_CHANGED_CHARACT_PRESENT  1                                          /**< Include or not the service_changed characteristic. if not enabled, the server's database cannot be changed for the lifetime of the device*/

#define DEVICE_NAME                      "Respeck_SV02"                               /**< Name of device. Will be included in the advertising data. */
#define MANUFACTURER_NAME                "Specknet Ltd"                            /**< Manufacturer. Will be passed to Device Information Service. */
#define APP_ADV_INTERVAL                 129                                       /**< The advertising interval (in units of 0.625 ms. This value corresponds to 25 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS       30                                        /**< The advertising timeout in units of seconds. */
#define APP_ADV_INTERVAL_SLOW                 1659                                 /**< The advertising interval (in units of 0.625 ms. This value corresponds to 25 ms). */
#define APP_ADV_TIMEOUT_SLOW_IN_SECONDS       30                                   /**< The advertising timeout in units of seconds. */

#define APP_TIMER_PRESCALER              0                                          /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE          6                                          /**< Size of timer operation queues. */

#define AVERAGING_INTERVAL               APP_TIMER_TICKS(60000, APP_TIMER_PRESCALER) /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define SUPERVISOR_INTERVAL              APP_TIMER_TICKS(70000, APP_TIMER_PRESCALER) /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */

// Supervisor INTERVAL must be higher than AVERAGING_INTERVAL, and higher than any event interval that is being supervised!

#define MIN_CONN_INTERVAL                MSEC_TO_UNITS(100, UNIT_1_25_MS)           /**< Minimum acceptable connection interval (0.1 seconds). */
#define MAX_CONN_INTERVAL                MSEC_TO_UNITS(200, UNIT_1_25_MS)           /**< Maximum acceptable connection interval (0.2 second). */
#define SLAVE_LATENCY                    0                                          /**< Slave latency. */
#define CONN_SUP_TIMEOUT                 MSEC_TO_UNITS(4000, UNIT_10_MS)            /**< Connection supervisory timeout (4 seconds). */

#define MIN_CONN_INTERVAL_SLOW               (UNIT_1_25_MS/16)           /**< Minimum acceptable connection interval (0.1 seconds). */
#define MAX_CONN_INTERVAL_SLOW               (UNIT_1_25_MS/8)           /**< Maximum acceptable connection interval (0.2 second). */
#define SLAVE_LATENCY_SLOW                    1                                          /**< Slave latency. */
#define CONN_SUP_TIMEOUT_SLOW                 MSEC_TO_UNITS(4000, UNIT_10_MS)            /**< Connection supervisory timeout (4 seconds). */


#define FIRST_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER) /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY    APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER)/**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT     3                                          /**< Number of attempts before giving up the connection parameter negotiation. */

#define SEC_PARAM_BOND                   1                                          /**< Perform bonding. */
#define SEC_PARAM_MITM                   0                                          /**< Man In The Middle protection not required. */
#define SEC_PARAM_IO_CAPABILITIES        BLE_GAP_IO_CAPS_NONE                       /**< No I/O capabilities. */
#define SEC_PARAM_OOB                    0                                          /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE           7                                          /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE           16                                         /**< Maximum encryption key size. */

#define DEAD_BEEF                        0xDEADBEEF                                 /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */
#ifdef BLE_DFU_APP_SUPPORT
#define DFU_REV_MAJOR                    0x00                                       /** DFU Major revision number to be exposed. */
#define DFU_REV_MINOR                    0x01                                       /** DFU Minor revision number to be exposed. */
#define DFU_REVISION                     ((DFU_REV_MAJOR << 8) | DFU_REV_MINOR)     /** DFU Revision number to be exposed. Combined of major and minor versions. */
#define APP_SERVICE_HANDLE_START         0x000C                                     /**< Handle of first application specific service when when service changed characteristic is present. */
#define BLE_HANDLE_MAX                   0xFFFF                                     /**< Max handle value in BLE. */

STATIC_ASSERT(IS_SRVC_CHANGED_CHARACT_PRESENT);                                     /** When having DFU Service support in application the Service Changed Characteristic should always be present. */
#endif // BLE_DFU_APP_SUPPORT

APP_TIMER_DEF(m_averaging_timer_id);
APP_TIMER_DEF(m_supervisor_timer_id);

static dm_application_instance_t        m_app_handle;                               /**< Application identifier allocated by device manager */

static uint16_t                          m_conn_handle = BLE_CONN_HANDLE_INVALID;   /**< Handle of the current connection. */

/* YOUR_JOB: Declare all services structure your application is using
static ble_xx_service_t                     m_xxs;
static ble_yy_service_t                     m_yys;
*/

// YOUR_JOB: Use UUIDs for service(s) used in your application.
static ble_uuid_t m_adv_uuids[] = {{BLE_UUID_DEVICE_INFORMATION_SERVICE, BLE_UUID_TYPE_BLE}}; /**< Universally unique service identifiers. */

#ifdef BLE_DFU_APP_SUPPORT
static ble_dfu_t                         m_dfus;                                    /**< Structure used to identify the DFU service. */
#endif // BLE_DFU_APP_SUPPORT

static ble_res_t                         m_res;

uint8_t* new_samples = NULL;
int new_samples_cnt = 0;

int valid_breathing_timeout_count = 0;
#define VALID_BREATHING_TIMEOUT 3

threshold_filter thresholds;
bpm_filter bpm;
breathing_filter breathing;
median_average_filter maf;
ma_stats_filter online_stats;

bool offline_state = false;
bool offline_enabled = false;
bool sleep_state = false;

#define BREATHS_FIFO_SIZE 4096

uint8_t breaths_buffer[BREATHS_FIFO_SIZE];
app_fifo_t breaths_buffer_fifo;

#define AVG_BREATHS_FIFO_SIZE 8192

uint8_t avg_breaths_buffer[AVG_BREATHS_FIFO_SIZE];
app_fifo_t avg_breaths_buffer_fifo;

#define LIVE_DATA_FIFO_SIZE 256

uint8_t live_data_buffer[LIVE_DATA_FIFO_SIZE];
app_fifo_t live_data_buffer_fifo;

#ifdef DEBUG
uint32_t m_error_code;
uint32_t m_line_num;
const uint8_t * m_p_file_name;

void app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t * p_file_name) {
    volatile bool loop = true;
    m_error_code = error_code;
    m_line_num = line_num;
    m_p_file_name = p_file_name;
    __disable_irq();

    while(loop);
}

#endif // DEBUG

extern online_activity_filter activity;
bool sv_averagecalled = false;
bool sv_accelerometer_samples_received = false;

#define CRV_SECONDS (150) // more than double the supervisory timeout, to cope with potential race conditions



/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num   Line number of the failing ASSERT call.
 * @param[in] file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    //ble_debug_assert_handler(error_code, line_num, p_file_name);
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

bool state_flag_to_sleep = false;

static void state_to_sleep_handle(void)
{
    state_flag_to_sleep = false;
    sleep_state = true;
    if (offline_state) {
        offline_state = false;
    }
    mma8451_init_sleep();
}

static void state_to_sleep(void)
{
    if (m_conn_handle == BLE_CONN_HANDLE_INVALID) {
        state_flag_to_sleep = true;
    }
}

static void state_to_offline(void)
{
    if (offline_enabled) {
        valid_breathing_timeout_count = 0;
        offline_state = true;
        MA_stats_init(&online_stats);
        Online_Activity_init(&activity);
        mma8451_start();
    }

}

bool check_avg_buffer(uint32_t size) {
    uint32_t remaining;
    app_fifo_write(&avg_breaths_buffer_fifo, NULL, &remaining);
    if (remaining > size + 1) {
        return true;
    } else {
        offline_enabled = false;
        state_to_sleep();
        return false;
    }
}

void start_new_average_sequence(void) {
    uint32_t ts;

    if (check_avg_buffer(6)) {
        app_fifo_put(&avg_breaths_buffer_fifo, 0xFF);
        ts = timestamp_get();
        app_fifo_put(&avg_breaths_buffer_fifo, (ts >> 24) & 0xFF);
        app_fifo_put(&avg_breaths_buffer_fifo, (ts >> 16) & 0xFF);
        app_fifo_put(&avg_breaths_buffer_fifo, (ts >> 8) & 0xFF);
        app_fifo_put(&avg_breaths_buffer_fifo, (ts >> 0) & 0xFF);
        app_fifo_put(&avg_breaths_buffer_fifo, 0);
    }
}

bool state_flag_disconnect = false;

static void state_disconnect_handle(void)
{
    state_flag_disconnect = false;
    if (offline_enabled) {
        start_new_average_sequence();
        state_to_offline();
    } else {
        state_to_sleep();
    }
}

static void state_disconnect(void)
{
    state_flag_disconnect = true;
}

bool state_flag_to_connected = false;

static void state_to_connected(void)
{
    state_flag_to_connected = false;

    offline_state = false;
    mma8451_start();
    MA_stats_init(&online_stats);
    Online_Activity_init(&activity);
    if (offline_enabled) {
        //offline_enabled = false;
        start_new_average_sequence();
    }
}

static void state_connect(void)
{
    state_flag_to_connected = true;
}

bool state_flag_movementdetected = false;

bool is_advertising = false;

static void state_movementdetected_handle(void)
{
    uint32_t err_code;
    state_flag_movementdetected = false;

    if (m_conn_handle == BLE_CONN_HANDLE_INVALID) {
        if (offline_enabled) {
            state_to_offline();
        }
        if(!is_advertising) {
            err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
            //APP_ERROR_CHECK(err_code);
        }
    } else {
        valid_breathing_timeout_count = 0;
        mma8451_start();
    }
}

static void state_movementdetected(void) {
    state_flag_movementdetected = true;
}

static void state_handler(void)
{
    if(state_flag_to_connected) {
        state_to_connected();
    }
    if(state_flag_disconnect) {
        state_disconnect_handle();
    }
    if(state_flag_movementdetected) {
        state_movementdetected_handle();
    }
    if(state_flag_to_sleep) {
        state_to_sleep_handle();
    }
}

bool check_live_buffer(uint32_t size) {
    uint32_t remaining;
    app_fifo_write(&live_data_buffer_fifo, NULL, &remaining);
    if (remaining > size) {
        return true;
    } else {
        return false;
    }
}

bool check_breaths_buffer(uint32_t size) {
    uint32_t remaining;
    app_fifo_write(&breaths_buffer_fifo, NULL, &remaining);
    if (remaining > size) {
        return true;
    } else {
        state_to_sleep();
        return false;
    }
}

static void averaging_timeout_handler(void *p_context)
{
    uint16_t activity_max;
    //start_new_average_sequence();
    sv_averagecalled = true;
    //nrf_gpio_pin_toggle(13);
    if ((MA_stats_num(&online_stats) > 0) || (Online_Activity_max(&activity) > 0.03)) {
        MA_stats_calculate(&online_stats);
        if (valid_breathing_timeout_count > 0) {
            valid_breathing_timeout_count = 0;
            start_new_average_sequence();
        }
        if (!check_avg_buffer(6)) {
            return;
        }
        app_fifo_put(&avg_breaths_buffer_fifo, 0xFE);
        app_fifo_put(&avg_breaths_buffer_fifo, MA_stats_num(&online_stats));
        app_fifo_put(&avg_breaths_buffer_fifo, MA_stats_mean(&online_stats) * 5.0);
        app_fifo_put(&avg_breaths_buffer_fifo, MA_stats_td(&online_stats) * 10.0);
        activity_max = Online_Activity_max(&activity) * 1000.0;
        app_fifo_put(&avg_breaths_buffer_fifo, (activity_max >> 8) &0xFF);
        app_fifo_put(&avg_breaths_buffer_fifo, (activity_max) &0xFF);
        //MA_stats_init(&online_stats);
        //Online_Activity_init(&activity);
    } else {
        valid_breathing_timeout_count++;
        if (valid_breathing_timeout_count >= VALID_BREATHING_TIMEOUT) {
            state_to_sleep();
        }
    }
    MA_stats_init(&online_stats);
    Online_Activity_init(&activity);
}

void supervisor_timeout_handler(void *p_context) {
    //nrf_gpio_pin_toggle(13);
    if (!sv_averagecalled) { // average should be active in all states
        nrf_gpio_cfg_output(13);
        nrf_gpio_pin_set(13);
        return;
    } else {
        sv_averagecalled = false;
    }
    if (offline_state && sv_averagecalled && sv_accelerometer_samples_received) {
        NRF_WDT->RR[0] = WDT_RR_RR_Reload;
        nrf_gpio_pin_clear(13);
    } else if (sleep_state) {
        NRF_WDT->RR[0] = WDT_RR_RR_Reload;
        nrf_gpio_pin_clear(13);
    } else if (sv_accelerometer_samples_received) {
        sv_accelerometer_samples_received = false;
        nrf_gpio_pin_clear(13);
        NRF_WDT->RR[0] = WDT_RR_RR_Reload;
    } else {
        nrf_gpio_cfg_output(13);
        nrf_gpio_pin_set(13);
    }
}

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static void timers_init(void)
{
    uint32_t err_code;
    // Initialize timer module.
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);
    err_code = app_timer_create(&m_averaging_timer_id, APP_TIMER_MODE_REPEATED, averaging_timeout_handler);
    APP_ERROR_CHECK(err_code);
    err_code = app_timer_create(&m_supervisor_timer_id, APP_TIMER_MODE_REPEATED, supervisor_timeout_handler);
    APP_ERROR_CHECK(err_code);

}


/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    /* YOUR_JOB: Use an appearance value matching the application's use case.
    err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_);
    APP_ERROR_CHECK(err_code); */

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


#ifdef BLE_DFU_APP_SUPPORT
/**@brief Function for stopping advertising.
 */
static void advertising_stop(void)
{
    uint32_t err_code;

    err_code = sd_ble_gap_adv_stop();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for loading application-specific context after establishing a secure connection.
 *
 * @details This function will load the application context and check if the ATT table is marked as
 *          changed. If the ATT table is marked as changed, a Service Changed Indication
 *          is sent to the peer if the Service Changed CCCD is set to indicate.
 *
 * @param[in] p_handle The Device Manager handle that identifies the connection for which the context
 *                     should be loaded.
 */
static void app_context_load(dm_handle_t const * p_handle)
{
    uint32_t                 err_code;
    static uint32_t          context_data;
    dm_application_context_t context;

    context.len    = sizeof(context_data);
    context.p_data = (uint8_t *)&context_data;

    err_code = dm_application_context_get(p_handle, &context);
    if (err_code == NRF_SUCCESS)
    {
        // Send Service Changed Indication if ATT table has changed.
        if ((context_data & (DFU_APP_ATT_TABLE_CHANGED << DFU_APP_ATT_TABLE_POS)) != 0)
        {
            err_code = sd_ble_gatts_service_changed(m_conn_handle, APP_SERVICE_HANDLE_START, BLE_HANDLE_MAX);
            if ((err_code != NRF_SUCCESS) &&
                (err_code != BLE_ERROR_INVALID_CONN_HANDLE) &&
                (err_code != NRF_ERROR_INVALID_STATE) &&
                (err_code != BLE_ERROR_NO_TX_BUFFERS) &&
                (err_code != NRF_ERROR_BUSY) &&
                (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING))
            {
                APP_ERROR_HANDLER(err_code);
            }
        }

        err_code = dm_application_context_delete(p_handle);
        APP_ERROR_CHECK(err_code);
    }
    else if (err_code == DM_NO_APP_CONTEXT)
    {
        // No context available. Ignore.
    }
    else
    {
        APP_ERROR_HANDLER(err_code);
    }
}


/** @snippet [DFU BLE Reset prepare] */
/**@brief Function for preparing for system reset.
 *
 * @details This function implements @ref dfu_app_reset_prepare_t. It will be called by
 *          @ref dfu_app_handler.c before entering the bootloader/DFU.
 *          This allows the current running application to shut down gracefully.
 */
static void reset_prepare(void)
{
    uint32_t err_code;

    if (m_conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        // Disconnect from peer.
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        APP_ERROR_CHECK(err_code);
    }
    else
    {
        // If not connected, the device will be advertising. Hence stop the advertising.
        advertising_stop();
    }

    err_code = ble_conn_params_stop();
    APP_ERROR_CHECK(err_code);

    nrf_delay_ms(500);
}
/** @snippet [DFU BLE Reset prepare] */
#endif // BLE_DFU_APP_SUPPORT

bool breath_interval_notifying;

void ble_res_handler (ble_res_t * p_hrs, ble_res_evt_t * p_evt)
{
    if (p_evt->evt_type == BLE_RES_EVT_OFFLINE_ENABLED) {
        offline_enabled = true;
    } else if (p_evt->evt_type == BLE_RES_EVT_OFFLINE_DISABLED) {
        offline_enabled = false;
    } else if (p_evt->evt_type == BLE_RES_EVT_SUPERVISOR_TEST) {
        app_timer_stop(m_averaging_timer_id);
    } else if (p_evt->evt_type == BLE_RES_EVT_SUPERVISOR_TEST_ACCEL) {
        mma8451_stop();
    }
}

uint8_t device_id[8];

/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    uint32_t       err_code;
    ble_dis_init_t dis_init;
    ble_res_init_t res_init;

    #ifdef BLE_DFU_APP_SUPPORT

        /** @snippet [DFU BLE Service initialization] */
        ble_dfu_init_t   dfus_init;

        // Initialize the Device Firmware Update Service.
        memset(&dfus_init, 0, sizeof(dfus_init));

        dfus_init.evt_handler   = dfu_app_on_dfu_evt;
        dfus_init.error_handler = NULL;
        dfus_init.evt_handler   = dfu_app_on_dfu_evt;
        dfus_init.revision      = DFU_REVISION;

        err_code = ble_dfu_init(&m_dfus, &dfus_init);
        APP_ERROR_CHECK(err_code);

        dfu_app_reset_prepare_set(reset_prepare);
        dfu_app_dm_appl_instance_set(m_app_handle);
        /** @snippet [DFU BLE Service initialization] */
    #endif // BLE_DFU_APP_SUPPORT

    // Initialize Device Information Service.
    memset(&dis_init, 0, sizeof(dis_init));

    ble_srv_ascii_to_utf8(&dis_init.manufact_name_str, (char *)MANUFACTURER_NAME);

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_init.dis_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&dis_init.dis_attr_md.write_perm);

    err_code = ble_dis_init(&dis_init);
    APP_ERROR_CHECK(err_code);
    device_id[0] = (NRF_FICR->DEVICEID[0]>>24) & 0xFF;
    device_id[1] = (NRF_FICR->DEVICEID[0]>>16) & 0xFF;
    device_id[2] = (NRF_FICR->DEVICEID[0]>>8) & 0xFF;
    device_id[3] = (NRF_FICR->DEVICEID[0]) & 0xFF;
    device_id[4] = (NRF_FICR->DEVICEID[1]>>24) & 0xFF;
    device_id[5] = (NRF_FICR->DEVICEID[1]>>16) & 0xFF;
    device_id[6] = (NRF_FICR->DEVICEID[1]>>8) & 0xFF;
    device_id[7] = (NRF_FICR->DEVICEID[1]) & 0xFF;


    memset(&res_init, 0, sizeof(res_init));
    res_init.evt_handler                 = ble_res_handler;
    res_init.p_device_id = device_id;
    res_init.p_timestamp_offset = &timestamp_offset;
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&res_init.res_res_attr_md.cccd_write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&res_init.res_res_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&res_init.res_res_attr_md.write_perm);

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&res_init.res_bsl_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&res_init.res_bsl_attr_md.write_perm);

    err_code = ble_res_init(&m_res, &res_init);
    APP_ERROR_CHECK(err_code);

}


/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;
    ble_gap_conn_params_t conn_params;

    memset(&cp_init, 0, sizeof(cp_init));
    memset(&conn_params, 0, sizeof(conn_params));
    conn_params.min_conn_interval = MIN_CONN_INTERVAL_SLOW;
    conn_params.max_conn_interval = MAX_CONN_INTERVAL_SLOW;
    conn_params.slave_latency = SLAVE_LATENCY_SLOW;
    conn_params.conn_sup_timeout = CONN_SUP_TIMEOUT_SLOW;

    cp_init.p_conn_params                  = &conn_params;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for starting timers.
*/
static void application_timers_start(void)
{
    timestamp_init();
    timestamp_config(197);

    uint32_t err_code;
    err_code = app_timer_start(m_averaging_timer_id, AVERAGING_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
    err_code = app_timer_start(m_supervisor_timer_id, SUPERVISOR_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);

}

/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            is_advertising = true;
            break;
        case BLE_ADV_EVT_IDLE:

            if (offline_state) {
                is_advertising = true;
                ble_advertising_start(BLE_ADV_MODE_SLOW);
            } else {
                is_advertising = false;
                state_to_sleep();
            }
            break;
        default:
            break;
    }
}

uint32_t live_packet_size = 0;
uint8_t live_packet[20];

uint8_t live_seq_number = 0;
uint8_t interval_seq_number = 0;
uint8_t average_seq_number = 0;

uint32_t interval_packet_size;
uint8_t interval_packet[20];

uint32_t average_packet_size;
uint8_t average_packet[20];

void send_data(void) {
    uint32_t result;
    bool buffer_not_full;
    buffer_not_full = true;

    if (buffer_not_full) {
        if ((m_conn_handle != BLE_CONN_HANDLE_INVALID) && m_res.respiratory_averages_notifying) {
            if (average_packet_size == 0) {
                average_packet_size = 18;
                app_fifo_read(&avg_breaths_buffer_fifo, &average_packet[1], &average_packet_size);
            }
            if(average_packet_size > 0) {
                average_packet[0] = average_seq_number;
                result = ble_res_send_breath_averages(&m_res, average_packet, average_packet_size+1);
                if (result == NRF_SUCCESS) {
                    average_packet_size = 0;
                    average_seq_number++;
                } else {
                    buffer_not_full = false;
                }
            }
        }
    }
    if (buffer_not_full) {
        if ((m_conn_handle != BLE_CONN_HANDLE_INVALID) && m_res.respiratory_interval_notifying) {
            if (interval_packet_size == 0) {
                interval_packet_size = MAX_PACKET_LENGTH - 1;
                app_fifo_read(&breaths_buffer_fifo, &interval_packet[1], &interval_packet_size);
            }
            if(interval_packet_size > 0) {
                interval_packet[0] = interval_seq_number;
                result = ble_res_send_breaths(&m_res, interval_packet, interval_packet_size+1);
                if (result == NRF_SUCCESS) {
                    interval_packet_size = 0;
                    interval_seq_number++;
                } else {
                    buffer_not_full = false;
                }
            }
        }
    }
    while (buffer_not_full) {
        buffer_not_full = false;
        if ((m_conn_handle != BLE_CONN_HANDLE_INVALID) && m_res.live_data_notifying) {
            if (live_packet_size == 0) {
                live_packet_size = 14;
                app_fifo_read(&live_data_buffer_fifo, &live_packet[1], &live_packet_size);
                live_packet[0] = live_seq_number;
            }
            if(live_packet_size > 0) {
                buffer_not_full = true;
                result = ble_res_send_live_data(&m_res, live_packet, live_packet_size+1);
                if (result == NRF_SUCCESS) {
                    live_packet_size = 0;
                    live_seq_number++;
                } else {
                    buffer_not_full = false;
                }
            }
        }
    }
}



/**@brief Function for handling the Application's BLE Stack events.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt)
{

    switch (p_ble_evt->header.evt_id)
            {
        case BLE_GAP_EVT_CONNECTED:
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            state_connect();
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            state_disconnect();
            ble_advertising_start(BLE_ADV_MODE_FAST);
            break;
        case BLE_EVT_TX_COMPLETE:
            send_data();
            break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the BLE Stack event interrupt handler after a BLE stack
 *          event has been received.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
    dm_ble_evt_handler(p_ble_evt);
    ble_conn_params_on_ble_evt(p_ble_evt);
    on_ble_evt(p_ble_evt);
    ble_advertising_on_ble_evt(p_ble_evt);
    ble_res_on_ble_evt(&m_res, p_ble_evt);
}


/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the System event interrupt handler after a system
 *          event has been received.
 *
 * @param[in] sys_evt  System stack event.
 */
static void sys_evt_dispatch(uint32_t sys_evt)
{
    pstorage_sys_event_handler(sys_evt);
    ble_advertising_on_sys_evt(sys_evt);
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    uint32_t err_code;

    // Initialize the SoftDevice handler module.
    SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_XTAL_30_PPM, NULL);

#if defined(S110) || defined(S130) || defined(S132)
    // Enable BLE stack.
    ble_enable_params_t ble_enable_params;
    memset(&ble_enable_params, 0, sizeof(ble_enable_params));
#if (defined(S130) || defined(S132))
    ble_enable_params.gatts_enable_params.attr_tab_size   = BLE_GATTS_ATTR_TAB_SIZE_DEFAULT;
#endif
    ble_enable_params.gatts_enable_params.service_changed = IS_SRVC_CHANGED_CHARACT_PRESENT;
    err_code = sd_ble_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);
#endif

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling the Device Manager events.
 *
 * @param[in] p_evt  Data associated to the device manager event.
 */
static uint32_t device_manager_evt_handler(dm_handle_t const * p_handle,
                                           dm_event_t const  * p_event,
                                           ret_code_t        event_result)
{
    APP_ERROR_CHECK(event_result);

#ifdef BLE_DFU_APP_SUPPORT
    if (p_event->event_id == DM_EVT_LINK_SECURED)
    {
        app_context_load(p_handle);
    }
#endif // BLE_DFU_APP_SUPPORT

    return NRF_SUCCESS;
}


/**@brief Function for the Device Manager initialization.
 *
 * @param[in] erase_bonds  Indicates whether bonding information should be cleared from
 *                         persistent storage during initialization of the Device Manager.
 */
static void device_manager_init(bool erase_bonds)
{
    uint32_t               err_code;
    dm_init_param_t        init_param = {.clear_persistent_data = erase_bonds};
    dm_application_param_t register_param;

    // Initialize persistent storage module.
    err_code = pstorage_init();
    APP_ERROR_CHECK(err_code);

    err_code = dm_init(&init_param);
    APP_ERROR_CHECK(err_code);

    memset(&register_param.sec_param, 0, sizeof(ble_gap_sec_params_t));

    register_param.sec_param.bond         = SEC_PARAM_BOND;
    register_param.sec_param.mitm         = SEC_PARAM_MITM;
    register_param.sec_param.io_caps      = SEC_PARAM_IO_CAPABILITIES;
    register_param.sec_param.oob          = SEC_PARAM_OOB;
    register_param.sec_param.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    register_param.sec_param.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
    register_param.evt_handler            = device_manager_evt_handler;
    register_param.service_type           = DM_PROTOCOL_CNTXT_GATT_SRVR_ID;

    err_code = dm_register(&m_app_handle, &register_param);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void)
{
    uint32_t      err_code;
    ble_advdata_t advdata;

    // Build advertising data struct to pass into @ref ble_advertising_init.
    memset(&advdata, 0, sizeof(advdata));

    advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    advdata.include_appearance      = true;
    advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    advdata.uuids_complete.p_uuids  = m_adv_uuids;

    ble_adv_modes_config_t options = {0};
    options.ble_adv_fast_enabled  = BLE_ADV_FAST_ENABLED;
    options.ble_adv_fast_interval = APP_ADV_INTERVAL;
    options.ble_adv_fast_timeout  = APP_ADV_TIMEOUT_IN_SECONDS;
    options.ble_adv_slow_enabled  = BLE_ADV_SLOW_ENABLED;
    options.ble_adv_slow_interval = APP_ADV_INTERVAL_SLOW;
    options.ble_adv_slow_timeout  = APP_ADV_TIMEOUT_SLOW_IN_SECONDS;

    err_code = ble_advertising_init(&advdata, NULL, &options, on_adv_evt, NULL);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for the Power manager.
 */
static void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}

void mma8451_samples_callback(uint8_t* samples, int length)
{
    sv_accelerometer_samples_received = true;
    if ((length == 0) && (samples == NULL)) {
        state_movementdetected();
    } else {
        new_samples = samples;
        new_samples_cnt = length;
    }
}

static bool valid_breath_sequence = false;

void start_new_breath_sequence(void) {
    uint32_t ts;
    if (!check_breaths_buffer(5)) {
        return;
    }
    app_fifo_put(&breaths_buffer_fifo, 0xFF);
    ts = timestamp_get();
    app_fifo_put(&breaths_buffer_fifo, (ts >> 24) & 0xFF);
    app_fifo_put(&breaths_buffer_fifo, (ts >> 16) & 0xFF);
    app_fifo_put(&breaths_buffer_fifo, (ts >> 8) & 0xFF);
    app_fifo_put(&breaths_buffer_fifo, (ts >> 0) & 0xFF);
}

void start_new_live_sequence(void) {
    uint32_t ts;
    if (!check_live_buffer(5)) {
        return;
    }
    app_fifo_put(&live_data_buffer_fifo, 0xFF);
    ts = timestamp_get();
    app_fifo_put(&live_data_buffer_fifo, (ts >> 24) & 0xFF);
    app_fifo_put(&live_data_buffer_fifo, (ts >> 16) & 0xFF);
    app_fifo_put(&live_data_buffer_fifo, (ts >> 8) & 0xFF);
    app_fifo_put(&live_data_buffer_fifo, (ts >> 0) & 0xFF);
    app_fifo_put(&live_data_buffer_fifo, 0);
    app_fifo_put(&live_data_buffer_fifo, 0);
    send_data();
}

void process_samples(void) {
    uint8_t bpm_int;
    uint32_t fifo_write_cnt;

    if (new_samples != NULL) {
        fifo_write_cnt = new_samples_cnt;
        if ((m_conn_handle != BLE_CONN_HANDLE_INVALID) && m_res.live_data_notifying) {
            start_new_live_sequence();
            for (int i = 0; i < new_samples_cnt; i+= 6) {
                if (check_live_buffer(7)) {
                    fifo_write_cnt = 6;
                    app_fifo_put(&live_data_buffer_fifo, 0xFE);
                    app_fifo_write(&live_data_buffer_fifo, &new_samples[i], &fifo_write_cnt);
                }
            }
        }
        if (offline_state || m_res.respiratory_interval_notifying) {
            for (int i = 0; i < new_samples_cnt; i+= 6) {
                float x = (float)((int16_t)((new_samples[i] << 8) | new_samples[i+1])) / 16384.0f;
                float y = (float)((int16_t)((new_samples[i+2] << 8) | new_samples[i+3])) / 16384.0f;
                float z = (float)((int16_t)((new_samples[i+4] << 8) | new_samples[i+5])) / 16384.0f;
                float accel[3] = {x, y, z};
                BRG_update(accel, &breathing);
                update_threshold(breathing.bs, &thresholds);


                float ut = thresholds.upper_value / 2.0f;
                float lt = thresholds.lower_value / 2.0f;
                bpm_update(breathing.bs, ut, lt, &bpm);
                if (bpm.updated && !isnan(bpm.bpm)) {
                    if(!valid_breath_sequence) {
                        start_new_breath_sequence();
                        valid_breath_sequence = true;
                    }
                    if (check_breaths_buffer(1)) {
                        app_fifo_put(&breaths_buffer_fifo, bpm.breath_sample_count);
                    }
                    MA_stats_update(bpm.bpm, &online_stats);
                    //MAVG_update(bpm.bpm, &maf);
                    //bpm_int = bpm.breath_sample_count;


                    bpm.updated = false;


                }
                if (bpm.updated && isnan(bpm.bpm)) {
                    valid_breath_sequence = false;
                }
            }

        }
        send_data();
        new_samples = NULL;
        new_samples_cnt = 0;
    }
}

/**@brief Function for application main entry.
 */
int main(void)
{
    uint32_t err_code;
    bool erase_bonds;
    erase_bonds = false;

    // Initialize.
    ble_stack_init();
    timers_init();
    device_manager_init(erase_bonds);
    gap_params_init();
    advertising_init();
    services_init();
    conn_params_init();
    nrf_gpio_cfg_output(13);

    mma8451_init(mma8451_samples_callback);

    app_fifo_init(&breaths_buffer_fifo, breaths_buffer, BREATHS_FIFO_SIZE);
    app_fifo_init(&avg_breaths_buffer_fifo, avg_breaths_buffer, AVG_BREATHS_FIFO_SIZE);
    app_fifo_init(&live_data_buffer_fifo, live_data_buffer, LIVE_DATA_FIFO_SIZE);

    // Start execution.
    application_timers_start();
    NRF_WDT->RREN = ( WDT_RREN_RR0_Enabled <<  WDT_RREN_RR0_Pos);
    NRF_WDT->RR[0] = WDT_RR_RR_Reload;
    NRF_WDT->CRV = 32768 * CRV_SECONDS;
    NRF_WDT->TASKS_START = 1;

    BRG_init(&breathing);
    threshold_init(&thresholds);
    bpm_init(&bpm);
    MAVG_init(&maf);
    MA_stats_init(&online_stats);
    //count = -1;
    //offset = 0;
    breathing.sample_rate = 13.0;
    breathing.sample_rate_valid = true;
    err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
    APP_ERROR_CHECK(err_code);

    mma8451_start();

    // Enter main loop.
    for (;;)
    {
        power_manage();
        process_samples();
        state_handler();
    }
}
