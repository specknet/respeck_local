#include <string.h>
#include "nrf_sdm.h"
#include "bootloader_types.h"
#include "nordic_common.h"
#include "ble_l2cap.h"
#include "ble_srv_common.h"
#include "app_util.h"
#include "ble_res.h"
#include "app_error.h"

/**@brief Function for handling the Connect event.
 *
 * @param[in]   p_res       Respiratory Rate Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_connect(ble_res_t * p_res, ble_evt_t * p_ble_evt)
{
    p_res->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}


/**@brief Function for handling the Disconnect event.
 *
 * @param[in]   p_res       Respiratory Rate Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_disconnect(ble_res_t * p_res, ble_evt_t * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_res->conn_handle = BLE_CONN_HANDLE_INVALID;
    p_res->respiratory_interval_notifying = false;
    p_res->respiratory_averages_notifying = false;
    p_res->live_data_notifying = false;
}


/**@brief Function for handling write events to the Respiratory Rate Measurement characteristic.
 *
 * @param[in]   p_res         Respiratory Rate Service structure.
 * @param[in]   p_evt_write   Write event received from the BLE stack.
 */
static void on_intervals_cccd_write(ble_res_t * p_res, ble_gatts_evt_write_t * p_evt_write)
{
    if (p_evt_write->len == 2)
    {
        // CCCD written, update notification state
        if (p_res->evt_handler != NULL)
        {
            ble_res_evt_t evt;

            if (ble_srv_is_notification_enabled(p_evt_write->data))
            {
                evt.evt_type = BLE_RES_EVT_INTERVALS_NOTIFICATION_ENABLED;
                p_res->respiratory_interval_notifying = true;
            }
            else
            {
                evt.evt_type = BLE_RES_EVT_INTERVALS_NOTIFICATION_DISABLED;
                p_res->respiratory_interval_notifying = false;
            }

            p_res->evt_handler(p_res, &evt);
        }
    }
}

static void on_averages_cccd_write(ble_res_t * p_res, ble_gatts_evt_write_t * p_evt_write)
{
    if (p_evt_write->len == 2)
    {
        // CCCD written, update notification state
        if (p_res->evt_handler != NULL)
        {
            ble_res_evt_t evt;

            if (ble_srv_is_notification_enabled(p_evt_write->data))
            {
                evt.evt_type = BLE_RES_EVT_AVERAGE_NOTIFICATION_ENABLED;
                p_res->respiratory_averages_notifying = true;
            }
            else
            {
                evt.evt_type = BLE_RES_EVT_AVERAGE_NOTIFICATION_DISABLED;
                p_res->respiratory_averages_notifying = false;
            }

            p_res->evt_handler(p_res, &evt);
        }
    }
}

static void on_timestamp_offset_value_write(ble_res_t * p_res, ble_gatts_evt_write_t * p_evt_write)
{
}
static void on_live_value_write(ble_res_t * p_res, ble_gatts_evt_write_t * p_evt_write)
{
    ble_res_evt_t evt;
    switch (p_evt_write->data[0]) {
        case 1:
            sd_power_gpregret_set(BOOTLOADER_DFU_START);
            //sd_softdevice_disable();
            sd_nvic_SystemReset();
            break;
        case 2:
            evt.evt_type = BLE_RES_EVT_OFFLINE_ENABLED;
            p_res->evt_handler(p_res, &evt);
            break;
        case 3:
            evt.evt_type = BLE_RES_EVT_OFFLINE_DISABLED;
            p_res->evt_handler(p_res, &evt);
            break;
        case 4:
            evt.evt_type = BLE_RES_EVT_SUPERVISOR_TEST;
            p_res->evt_handler(p_res, &evt);
            break;
        case 5:
            evt.evt_type = BLE_RES_EVT_SUPERVISOR_TEST_ACCEL;
            p_res->evt_handler(p_res, &evt);
            break;

    }
}
static void on_live_cccd_write(ble_res_t * p_res, ble_gatts_evt_write_t * p_evt_write)
{
    if (p_evt_write->len == 2)
    {
        // CCCD written, update notification state
        if (p_res->evt_handler != NULL)
        {
            ble_res_evt_t evt;

            if (ble_srv_is_notification_enabled(p_evt_write->data))
            {
                evt.evt_type = BLE_RES_EVT_NOTIFICATION_ENABLED;
                p_res->live_data_notifying = true;
            }
            else
            {
                evt.evt_type = BLE_RES_EVT_NOTIFICATION_DISABLED;
                p_res->live_data_notifying = false;
            }

            p_res->evt_handler(p_res, &evt);
        }
    }
}

/**@brief Function for handling the Write event.
 *
 * @param[in]   p_res       Respiratory Rate Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_write(ble_res_t * p_res, ble_evt_t * p_ble_evt)
{
    ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

    if (p_evt_write->handle == p_res->respiratory_interval_handles.cccd_handle)
    {
        on_intervals_cccd_write(p_res, p_evt_write);
    }
    if (p_evt_write->handle == p_res->respiratory_averages_handles.cccd_handle)
    {
        on_averages_cccd_write(p_res, p_evt_write);
    }
    if (p_evt_write->handle == p_res->live_handles.cccd_handle)
    {
        on_live_cccd_write(p_res, p_evt_write);
    }
    if (p_evt_write->handle == p_res->timestamp_offset_handles.value_handle)
    {
        on_timestamp_offset_value_write(p_res, p_evt_write);
    }
    if (p_evt_write->handle == p_res->live_handles.value_handle)
    {
        on_live_value_write(p_res, p_evt_write);
    }
}


void ble_res_on_ble_evt(ble_res_t * p_res, ble_evt_t * p_ble_evt)
{
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_res, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_res, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_res, p_ble_evt);
            break;

        default:
            // No implementation needed.
            break;
    }
}

static uint32_t timestamp_offset_char_add(ble_res_t            * p_res,
                                                const ble_res_init_t * p_res_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
    cccd_md.vloc       = BLE_GATTS_VLOC_STACK;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.read = 1;
    char_md.char_props.write = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    BLE_UUID_BLE_ASSIGN(ble_uuid, TIMESTAMP_OFFSET_DATA_UUID);

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    attr_md.vloc       = BLE_GATTS_VLOC_USER;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = 4;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = 4;
    attr_char_value.p_value   = (uint8_t*)p_res_init->p_timestamp_offset;

    return sd_ble_gatts_characteristic_add(p_res->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_res->timestamp_offset_handles);
}

static uint32_t device_id_char_add(ble_res_t            * p_res,
                                                const ble_res_init_t * p_res_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
    cccd_md.vloc       = BLE_GATTS_VLOC_STACK;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.read = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    BLE_UUID_BLE_ASSIGN(ble_uuid, DEVICE_ID_UUID);

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&attr_md.write_perm);
    attr_md.vloc       = BLE_GATTS_VLOC_USER;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = 8;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = 8;
    attr_char_value.p_value   = p_res_init->p_device_id;

    return sd_ble_gatts_characteristic_add(p_res->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_res->device_id_handles);
}

/**@brief Function for adding the Respiratory Rate Measurement characteristic.
 *
 * @param[in]   p_res        Respiratory Rate Service structure.
 * @param[in]   p_res_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t respiratory_interval_char_add(ble_res_t            * p_res,
                                                const ble_res_init_t * p_res_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    uint8_t initial_value[1] = {0xFF};

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    cccd_md.write_perm = p_res_init->res_res_attr_md.cccd_write_perm;
    cccd_md.vloc       = BLE_GATTS_VLOC_STACK;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.notify = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    BLE_UUID_BLE_ASSIGN(ble_uuid, RESPIRATORY_INTERVALS_UUID);

    memset(&attr_md, 0, sizeof(attr_md));

    attr_md.read_perm  = p_res_init->res_res_attr_md.read_perm;
    attr_md.write_perm = p_res_init->res_res_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = 1;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = MAX_INTERVALS_LENGTH;
    attr_char_value.p_value   = initial_value;

    return sd_ble_gatts_characteristic_add(p_res->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_res->respiratory_interval_handles);
}

static uint32_t live_data_char_add(ble_res_t            * p_res,
                                                const ble_res_init_t * p_res_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    uint8_t initial_value[1] = {0xFF};

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
    cccd_md.write_perm = p_res_init->res_res_attr_md.cccd_write_perm;
    cccd_md.vloc       = BLE_GATTS_VLOC_STACK;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.notify = 1;
    char_md.char_props.write = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    BLE_UUID_BLE_ASSIGN(ble_uuid, LIVE_ACCEL_DATA_UUID);

    memset(&attr_md, 0, sizeof(attr_md));

    //attr_md.read_perm  = p_res_init->res_res_attr_md.read_perm;
    //attr_md.write_perm = p_res_init->res_res_attr_md.write_perm;
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = 1;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = MAX_INTERVALS_LENGTH;
    attr_char_value.p_value   = initial_value;

    return sd_ble_gatts_characteristic_add(p_res->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_res->live_handles);
}

static uint32_t respiratory_averages_char_add(ble_res_t            * p_res,
                                                const ble_res_init_t * p_res_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    uint8_t initial_value[1] = {0xFF};

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    cccd_md.write_perm = p_res_init->res_res_attr_md.cccd_write_perm;
    cccd_md.vloc       = BLE_GATTS_VLOC_STACK;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.notify = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    BLE_UUID_BLE_ASSIGN(ble_uuid, AVERAGE_BREATHS_UUID);

    memset(&attr_md, 0, sizeof(attr_md));

    attr_md.read_perm  = p_res_init->res_res_attr_md.read_perm;
    attr_md.write_perm = p_res_init->res_res_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = 1;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = MAX_INTERVALS_LENGTH;
    attr_char_value.p_value   = initial_value;

    return sd_ble_gatts_characteristic_add(p_res->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_res->respiratory_averages_handles);
}

uint32_t ble_res_send_breaths(ble_res_t * p_res, uint8_t *data, uint16_t length) {
    uint32_t err_code;
    ble_gatts_hvx_params_t hvx_params;
    uint16_t len = length;
    if (len > 20) len = 20;
    if ((p_res->conn_handle != BLE_CONN_HANDLE_INVALID) && (p_res->respiratory_interval_notifying)) {
        memset(&hvx_params, 0, sizeof(hvx_params));
        hvx_params.handle   = p_res->respiratory_interval_handles.value_handle;
        hvx_params.type     = BLE_GATT_HVX_NOTIFICATION;
        hvx_params.offset   = 0;
        hvx_params.p_len    = &len;
        hvx_params.p_data   = data;

        err_code = sd_ble_gatts_hvx(p_res->conn_handle, &hvx_params);
        if (err_code == BLE_ERROR_NO_TX_BUFFERS ||
            err_code == NRF_ERROR_INVALID_STATE ||
            err_code == BLE_ERROR_GATTS_SYS_ATTR_MISSING) {
            return NRF_ERROR_INVALID_STATE;
        } else if ((err_code == NRF_SUCCESS)) {

            //err_code = NRF_ERROR_DATA_SIZE;
        }
        //APP_ERROR_CHECK(err_code);
        return NRF_SUCCESS;
    }
    return NRF_SUCCESS;
}

uint32_t ble_res_send_live_data(ble_res_t * p_res, uint8_t *data, uint16_t length) {
    uint32_t err_code;
    ble_gatts_hvx_params_t hvx_params;
    uint16_t len = length;
    if (len > 20) len = 20;
    if ((p_res->conn_handle != BLE_CONN_HANDLE_INVALID) && (p_res->live_data_notifying)) {
        memset(&hvx_params, 0, sizeof(hvx_params));
        hvx_params.handle   = p_res->live_handles.value_handle;
        hvx_params.type     = BLE_GATT_HVX_NOTIFICATION;
        hvx_params.offset   = 0;
        hvx_params.p_len    = &len;
        hvx_params.p_data   = data;

        err_code = sd_ble_gatts_hvx(p_res->conn_handle, &hvx_params);
        if (err_code == BLE_ERROR_NO_TX_BUFFERS ||
            err_code == NRF_ERROR_INVALID_STATE ||
            err_code == BLE_ERROR_GATTS_SYS_ATTR_MISSING) {
            return NRF_ERROR_INVALID_STATE;
        } else if ((err_code == NRF_SUCCESS)) {

            //err_code = NRF_ERROR_DATA_SIZE;
        }
        //APP_ERROR_CHECK(err_code);
        return NRF_SUCCESS;
    }
    return NRF_SUCCESS;
}

uint32_t ble_res_send_breath_averages(ble_res_t * p_res, uint8_t *data, uint16_t length) {
    uint32_t err_code;
    ble_gatts_hvx_params_t hvx_params;
    uint16_t len = length;
    if (len > 20) len = 20;
    if ((p_res->conn_handle != BLE_CONN_HANDLE_INVALID) && (p_res->respiratory_averages_notifying)) {
        memset(&hvx_params, 0, sizeof(hvx_params));
        hvx_params.handle   = p_res->respiratory_averages_handles.value_handle;
        hvx_params.type     = BLE_GATT_HVX_NOTIFICATION;
        hvx_params.offset   = 0;
        hvx_params.p_len    = &len;
        hvx_params.p_data   = data;

        err_code = sd_ble_gatts_hvx(p_res->conn_handle, &hvx_params);
        if (err_code == BLE_ERROR_NO_TX_BUFFERS ||
            err_code == NRF_ERROR_INVALID_STATE ||
            err_code == BLE_ERROR_GATTS_SYS_ATTR_MISSING) {
            return NRF_ERROR_INVALID_STATE;
        } else if ((err_code == NRF_SUCCESS)) {

            //err_code = NRF_ERROR_DATA_SIZE;
        }
        //APP_ERROR_CHECK(err_code);
        return NRF_SUCCESS;
    }
    return NRF_SUCCESS;
}

uint32_t ble_res_init(ble_res_t * p_res, const ble_res_init_t * p_res_init)
{
    uint32_t   err_code;
    ble_uuid_t ble_uuid;
    ble_uuid128_t res_base_uuid = RESPECK_BASE_UUID;

    // Initialize service structure
    p_res->evt_handler                 = p_res_init->evt_handler;
    p_res->conn_handle                 = BLE_CONN_HANDLE_INVALID;

    /**@snippet [Adding proprietary Service to S110 SoftDevice] */
    // Add a custom base UUID.
    err_code = sd_ble_uuid_vs_add(&res_base_uuid, &p_res->uuid_type);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    ble_uuid.type = p_res->uuid_type;
    ble_uuid.uuid = RESPECK_SERVICE_UUID;
    // Add service
    //BLE_UUID_BLE_ASSIGN(ble_uuid, RESPECK_SERVICE_UUID);

    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                        &ble_uuid,
                                        &p_res->service_handle);

    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add Respiratory rate measurement characteristic
    err_code = live_data_char_add(p_res, p_res_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    err_code = timestamp_offset_char_add(p_res, p_res_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    err_code = device_id_char_add(p_res, p_res_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add Respiratory rate measurement characteristic
    err_code = respiratory_interval_char_add(p_res, p_res_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    // Add Respiratory rate measurement characteristic
    err_code = respiratory_averages_char_add(p_res, p_res_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    return NRF_SUCCESS;
}
