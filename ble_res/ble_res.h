#ifndef BLE_RES_H__
#define BLE_RES_H__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"
#include "uuid.h"

#define MAX_INTERVALS_LENGTH 20
#define MAX_PACKET_LENGTH 20



/**@brief Respiratory Rate Service event type. */
typedef enum
{
    BLE_RES_EVT_NOTIFICATION_ENABLED,                   /**< Respiratory Rate value notification enabled event. */
    BLE_RES_EVT_NOTIFICATION_DISABLED,                   /**< Respiratory Rate value notification disabled event. */
    BLE_RES_EVT_AVERAGE_NOTIFICATION_ENABLED,
    BLE_RES_EVT_AVERAGE_NOTIFICATION_DISABLED,
    BLE_RES_EVT_INTERVALS_NOTIFICATION_ENABLED,
    BLE_RES_EVT_INTERVALS_NOTIFICATION_DISABLED,
    BLE_RES_EVT_OFFLINE_ENABLED,
    BLE_RES_EVT_OFFLINE_DISABLED,
    BLE_RES_EVT_SUPERVISOR_TEST,
    BLE_RES_EVT_SUPERVISOR_TEST_ACCEL
} ble_res_evt_type_t;

/**@brief Respiratory Rate Service event. */
typedef struct
{
    ble_res_evt_type_t evt_type;                        /**< Type of event. */
} ble_res_evt_t;

// Forward declaration of the ble_res_t type.
typedef struct ble_res_s ble_res_t;

/**@brief Respiratory Rate Service event handler type. */
typedef void (*ble_res_evt_handler_t) (ble_res_t * p_hrs, ble_res_evt_t * p_evt);

typedef void (*ble_res_timestamp_value_handler_t) (uint32_t timestamp_offset);

/**@brief Respiratory Rate Service init structure. This contains all options and data needed for
 *        initialization of the service. */
typedef struct
{
    ble_res_evt_handler_t        evt_handler;                                          /**< Event handler to be called for handling events in the Respiratory Rate Service. */
    ble_srv_cccd_security_mode_t res_res_attr_md;                                      /**< Initial security level for Respiratory rate service measurement attribute */
    ble_srv_security_mode_t      res_bsl_attr_md;                                      /**< Initial security level for body sensor location attribute */
    uint32_t*                    p_timestamp_offset;
    uint8_t*                     p_device_id;
} ble_res_init_t;

// Forward declaration of the ble_hrs_t type.
typedef struct ble_hrs_s ble_hrs_t;

/**@brief Respiratory Rate Service structure. This contains various status information for the service. */
struct ble_res_s
{
    ble_res_evt_handler_t        evt_handler;                                          /**< Event handler to be called for handling events in the Respiratory Rate Service. */
    uint8_t                      uuid_type;
    uint16_t                     service_handle;                                       /**< Handle of Respiratory Rate Service (as provided by the BLE stack). */
    bool                         live_data_notifying;
    bool                         respiratory_interval_notifying;
    bool                         respiratory_averages_notifying;
    uint32_t*                    p_timestamp_offset;
    uint8_t*                     p_device_id;
    ble_gatts_char_handles_t     live_handles;                                          /**< Handles related to the Respiratory Rate Measurement characteristic. */
    ble_gatts_char_handles_t     timestamp_offset_handles;                                          /**< Handles related to the Body Sensor Location characteristic. */
    ble_gatts_char_handles_t     device_id_handles;                                         /**< Handles related to the Respiratory Rate Control Point characteristic. */
    ble_gatts_char_handles_t     respiratory_interval_handles;                                         /**< Handles related to the Respiratory Rate Control Point characteristic. */
    ble_gatts_char_handles_t     respiratory_averages_handles;                                         /**< Handles related to the Respiratory Rate Control Point characteristic. */
    uint16_t                     conn_handle;                                          /**< Handle of the current connection (as provided by the BLE stack, is BLE_CONN_HANDLE_INVALID if not in a connection). */
};

/**@brief Function for initializing the Respiratory Rate Service.
 *
 * @param[out]  p_hrs       Respiratory Rate Service structure. This structure will have to be supplied by
 *                          the application. It will be initialized by this function, and will later
 *                          be used to identify this particular service instance.
 * @param[in]   p_res_init  Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on successful initialization of service, otherwise an error code.
 */
uint32_t ble_res_init(ble_res_t * p_hrs, const ble_res_init_t * p_res_init);

uint32_t ble_res_send_breaths(ble_res_t * p_res, uint8_t *data, uint16_t length);
uint32_t ble_res_send_live_data(ble_res_t * p_res, uint8_t *data, uint16_t length);
uint32_t ble_res_send_breath_averages(ble_res_t * p_res, uint8_t *data, uint16_t length);

/**@brief Function for handling the Application's BLE Stack events.
 *
 * @details Handles all events from the BLE stack of interest to the Respiratory Rate Service.
 *
 * @param[in]   p_res      Respiratory Rate Service structure.
 * @param[in]   p_ble_evt  Event received from the BLE stack.
 */
void ble_res_on_ble_evt(ble_res_t * p_res, ble_evt_t * p_ble_evt);

/**@brief Function for sending respiratory intervals if notification has been enabled.
 *
 * @details The application calls this function when new respiratory rate information is available.
 *          If notification has been enabled, the respiratory rate measurement data is encoded and sent to
 *          the client.
 *
 * @param[in]   p_res                    Respiratory Rate Service structure.
 * @param[in]   data               New respiratory rate measurement.
 * @param[in]   length             Length of data packet
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
//uint32_t ble_res_respiratory_rate_measurement_send(ble_res_t * p_res, uint8_t *data, int length);

#endif // BLE_RES_H__

/** @} */
