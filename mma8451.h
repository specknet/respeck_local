#ifndef MMA8451_H
#define MMA8451_H

#include <stdint.h>

//#define MMA8451_address 0x38
#define MMA8451_address 0x1C
#define MMA8451_INT_PIN 10

typedef void (* mma8451_callback_t)(uint8_t* data, int length);

void mma8451_init(mma8451_callback_t data_callback);
void mma8451_read(void);
void mma8451_stop(void);
void mma8451_start(void);
void mma8451_init_sleep(void);

#endif // MMA8451_H
