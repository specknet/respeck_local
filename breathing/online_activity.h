#ifndef __ONLINE_ACTIVITY_H__
#define __ONLINE_ACTIVITY_H__

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef struct
{
	int num_N;
    float prev_accel[3];
	float max;
	float activity;
} online_activity_filter;

void Online_Activity_init(online_activity_filter* filter);
void Online_Activity_update(float accel[3], online_activity_filter* filter);

int Online_Activity_num(online_activity_filter* filter);
float Online_Activity_max(online_activity_filter* filter);

#endif // __ONLINE_ACTIVITY_H__
