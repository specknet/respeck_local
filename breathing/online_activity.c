#include "online_activity.h"
#include <math.h>

void Online_Activity_init(online_activity_filter* filter)
{
    filter->num_N = 0;
}
void Online_Activity_update(float accel[3], online_activity_filter* filter)
{
    float power;
    filter->num_N++;

    if (filter->num_N == 1) {
        power = 0.0;
        filter->max = power;
    } else {
        power = sqrt((accel[0] - filter->prev_accel[0]) * (accel[0] - filter->prev_accel[0]) + (accel[1] - filter->prev_accel[1]) * (accel[1] - filter->prev_accel[1]) + (accel[2] - filter->prev_accel[2]) * (accel[2] - filter->prev_accel[2]));
        filter->activity = power;
        if (power > filter->max) {
            filter->max = power;
        }
    }
    filter->prev_accel[0] = accel[0];
    filter->prev_accel[1] = accel[1];
    filter->prev_accel[2] = accel[2];
}

int Online_Activity_num(online_activity_filter* filter)
{
    return filter->num_N;
}

float Online_Activity_max(online_activity_filter* filter)
{
    return (filter->num_N > 0) ? filter->max : 0.0;
}
