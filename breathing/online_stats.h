#ifndef __ONLINE_STATS_H__
#define __ONLINE_STATS_H__

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef struct
{
	int num_N;
	float oldM;
    float newM;
    float oldS;
    float newS;
	float max;
	float min;
} online_stats_filter;

void Online_init(online_stats_filter* filter);
void Online_update(float value, online_stats_filter* filter);

int Online_num(online_stats_filter* filter);
float Online_mean(online_stats_filter* filter);
float Online_var(online_stats_filter* filter);
float Online_td(online_stats_filter* filter);

#endif // __ONLINE_STATS_H__
