#include "online_stats.h"
#include <math.h>

int num_N;
float oldM;
float newM;
float oldS;
float newS;


void Online_init(online_stats_filter* filter)
{
    filter->num_N = 0;
}
void Online_update(float value, online_stats_filter* filter)
{
    filter->num_N++;
    if (filter->num_N == 1) {
        filter->oldM = filter->newM = value;
        filter->oldS = 0.0;
        filter->max = value;
        filter->min = value;
    } else {
        filter->newM = filter->oldM + (value - filter->oldM) / filter->num_N;
        filter->newS = filter->oldS + (value - filter->oldM) * (value - filter->newM);

        filter->oldM = filter->newM;
        filter->oldS = filter->newS;

        filter->max = (value > filter->max) ? value : filter->max;
        filter->min = (value < filter->min) ? value : filter->min;
    }
}

int Online_num(online_stats_filter* filter)
{
    return filter->num_N;
}

float Online_mean(online_stats_filter* filter)
{
    return (filter->num_N > 0) ? filter->newM : 0.0;
}

float Online_var(online_stats_filter* filter)
{
    return ( (filter->num_N > 1) ? filter->newS / (filter->num_N - 1) : 0.0);
}

float Online_td(online_stats_filter* filter)
{
    return sqrt(Online_var(filter));
}
