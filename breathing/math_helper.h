#ifndef __MATH_HELPER_H__
#define __MATH_HELPER_H__

#include <stdint.h>

void normalize(float vector[3]);
void normalize_int_to_float(int32_t *vector, float *u);
void normalize_f32(float *u);

void vector_copy_dbl(float in[3], float out[3]);
void vector_copy_int(int32_t in[3], int32_t out[3]);
float dot(float v[3], float u[3]);

#endif
