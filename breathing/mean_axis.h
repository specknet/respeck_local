#ifndef __MEAN_AXIS_H__
#define __MEAN_AXIS_H__

#include <stdbool.h>
#include <stdint.h>

#define MEAN_AXIS_SIZE 128

typedef struct
{

	float sum[3];
	int pos;
	int fill;

	float values[MEAN_AXIS_SIZE][3];
	float value[3];
	bool valid;

} mean_axis_filter;

void MAX_init(mean_axis_filter* filter);
void MAX_update(float data[3], mean_axis_filter* filter);

#endif
