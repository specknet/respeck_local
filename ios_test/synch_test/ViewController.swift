//
//  ViewController.swift
//  synch_test
//
//  Created by Janek Mann on 01/09/2015.
//  Copyright © 2015 specknet. All rights reserved.
//

import UIKit
import Foundation
import CoreBluetooth
import SceneKit

var btManager:CBCentralManager? = nil
var loggingEnabled = true

let RespeckLiveCharacteristicUUID = CBUUID(string: "2010")
let RespeckServiceUUID = CBUUID(string: "00001010-E117-4BFF-B00D-B20878BC3F44")

class Logger {
    var label: UILabel
    var log: [String] = []
    let logLength = 5
    
    init(_ label: UILabel) {
        self.label = label
    }
    func log(logString: String) {
        print(logString)
        log.append(logString)
        if (log.count > logLength) {
            log.removeAtIndex(0)
        }
        var logString = ""
        for string in log {
            logString = logString + "\n" + string
        }
        self.label.text = logString
    }
}

class ViewController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    var orients: [CBPeripheral] = []
    var peripheral: CBPeripheral?
    var accelCharacteristic: CBCharacteristic?
    var gyroCharacteristic: CBCharacteristic?
    var quatCharacteristic: CBCharacteristic?
    var refTime: Int32 = 0
    var filePath = ""
    var outputStream : NSOutputStream?
    var captureCount = 0
    var variableIndex = 0

    @IBOutlet var activityLogLabel: UILabel!
    @IBOutlet var dataLogLabel: UILabel!
    
    var logger: Logger! = nil
    var dataLogger: Logger! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logger = Logger(activityLogLabel)
        dataLogger = Logger(dataLogLabel)

        logger.log("Starting Log")
        

        if (btManager == nil) {
            btManager = CBCentralManager(delegate: self, queue: nil)

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func centralManagerDidUpdateState(central: CBCentralManager) {
        switch (central.state)
        {
        case .Unsupported:
            
            break
        case .Unauthorized:
            
            break
        case .PoweredOff:
            
            break
        case .PoweredOn:
            break
        case .Unknown:
            break
        default:
            break
        }
    }
    @IBAction func scanButton(sender: AnyObject) {
        logger.log("start scan")
        btManager!.scanForPeripheralsWithServices(nil, options:nil)
    }
    @IBAction func disconnectButtonPressed(sender: AnyObject) {
        logger.log("Disconnecting")
        for orient in orients {
            btManager!.cancelPeripheralConnection(orient)
        }
    }
    @IBAction func connectButton(sender: AnyObject) {
        let file = "orientCapture" + String(captureCount) + ".csv"
        logger.log("Capture: \(file)")
        captureCount++
        if let dir : NSString = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true).first {
            filePath = dir.stringByAppendingPathComponent(file);
            outputStream = NSOutputStream(toFileAtPath: filePath, append: false)
            outputStream?.open()
            
        }

        for orient in orients {
            btManager!.connectPeripheral(orient, options: nil)
        }
    }
    @IBAction func captureButton(sender: AnyObject) {
        for orient in orients {
            orient.discoverServices(nil)
        }
    }
    @IBAction func synchroniseButton(sender: AnyObject) {
    }
    func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
        
        //logger.log("detected peripheral: \(peripheral)")
        //logger.log("peripheral.name: \(peripheral.name)")
        if peripheral.name == "Respeck_L8" {
            //self.peripheral = peripheral
            orients.append(peripheral)
            logger.log("found respeck \(peripheral.identifier.hashValue)")
        }
        
    }
    
    func centralManager(central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        
        peripheral.delegate = nil
        logger.log("Disconnected from \(peripheral.identifier.hashValue)")
    }
    
    func centralManager(central: CBCentralManager!, didRetrievePeripherals peripherals: [AnyObject]!) {
    }
    
    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
        if loggingEnabled {logger.log("connected to peripheral: \(peripheral.identifier.hashValue)")}
        
        peripheral.delegate = self
        peripheral.discoverServices(nil)
    }
    
    func centralManager(central: CBCentralManager, didFailToConnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        logger.log("Failed to connect")
        
    }
    
    
    //MARK: CBPeripheralDelegate methods
    
    
    func peripheral(peripheral: CBPeripheral, didDiscoverServices error: NSError?) {
        
        logger.log("Discovered Services")
        for aService in peripheral.services! {
            
            let cbService = aService
            logger.log("Discovered Service: \(aService)")
            /* Orient Service */
            if aService.UUID == RespeckServiceUUID {
                logger.log("Discovered Respeck Service")
                peripheral.discoverCharacteristics(nil, forService: cbService)
            }
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverCharacteristicsForService service: CBService, error: NSError?) {
        for aCharacteristic  in service.characteristics! {
            let cbCharacteristic = aCharacteristic
            logger.log("Found characteristic \(cbCharacteristic)")
            if cbCharacteristic.UUID == RespeckLiveCharacteristicUUID {
                logger.log("Found Live characteristic")
                quatCharacteristic = cbCharacteristic
                peripheral.setNotifyValue(true, forCharacteristic: cbCharacteristic)
            }
        }
        
    }
    
    func floatFromData(data: NSData, range: NSRange) -> Float {
        var rawVal : UInt32 = 0
        data.getBytes(&rawVal, range: range)
        let signedVal = Int32(bitPattern: CFSwapInt32LittleToHost(rawVal))
        let divisor = (1 << 30)
        return (Float(signedVal) / Float(divisor))
        
    }
    
    func floatFromQ16(data: NSData, range: NSRange) -> Float {
        var rawVal : UInt32 = 0
        data.getBytes(&rawVal, range: range)
        let signedVal = Int32(bitPattern: CFSwapInt32LittleToHost(rawVal))
        let divisor = (1 << 16)
        return (Float(signedVal) / Float(divisor))
        
    }
    
    func openFile() {
    }
    
    func peripheral(peripheral: CBPeripheral, didUpdateValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        if (characteristic.UUID == RespeckLiveCharacteristicUUID) {
            let value = characteristic.value
            if (value!.length == 5) {
                var timestamp: UInt32 = 0
                value!.getBytes(&timestamp, range: NSRange(location: 1, length: 4))
                let unsignedVal = CFSwapInt32BigToHost(timestamp)
                dataLogger.log("TS: \(unsignedVal)")
                self.variableIndex = 0
            } else {
                for var index = 0; index < value!.length-1; index += 2 {
                    var data_val: UInt16 = 0
                    value!.getBytes(&data_val, range:NSRange(location: index, length: 2))
                    let signed_data_val = Int16(bitPattern: CFSwapInt16BigToHost(data_val))
                    let float_val = Double(signed_data_val) / 16384.0
                    dataLogger.log("\(self.variableIndex): \(float_val)")
                    self.variableIndex = (self.variableIndex + 1) % 3
                }
            }
            
        }
    }

}

