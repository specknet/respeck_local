#ifndef _CLOCK_H
#define _CLOCK_H

#include <stdint.h>

void timestamp_init(void);
void timestamp_config(uint32_t ticks);
void timestamp_reset(void);
uint32_t timestamp_get(void);
void timestamp_offset_set(uint32_t ticks);
uint32_t timestamp_offset_get(void);
extern uint32_t timestamp_offset;

#endif
